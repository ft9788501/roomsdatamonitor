﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RoomsDataMonitor.Destinations.Glip
{
    public class DestinationGlip : IDestination
    {
        private BlockingCollection<PostObject> destinationBuffer = new BlockingCollection<PostObject>();

        public DestinationGlip()
        {
            Task.Run(async () =>
            {
                foreach (var o in destinationBuffer.GetConsumingEnumerable())
                {
                    await SendAsync(o);
                }
            });
        }
        public void Send(string msg)
        {
            PostObject postObject = new PostObject() { body = msg };
            destinationBuffer.Add(postObject);
        }
        private async Task SendAsync(PostObject postObject)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.Timeout = TimeSpan.FromSeconds(10);
                StringContent stringContent = new StringContent(JsonConvert.SerializeObject(postObject));
                stringContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
                try
                {
                    var result = await httpClient.PostAsync(new Uri("https://hooks.glip.com/webhook/c52a439d-9363-4fce-a8a8-51daabf8c3c4"), stringContent);
                    if (!result.IsSuccessStatusCode)
                    {
                        throw new Exception($"ErrorStatusCode:{result.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    await Task.Delay(1000);
                    await SendAsync(postObject);
                }
            }
        }
        private class PostObject
        {
            public string icon { get; set; } = "http://tinyurl.com/pn46fgp";
            public string activity { get; set; } = "RoomsDataMonitor";
            public string title { get; set; } = "Receive data from the RoomsDataMonitor:";
            public string body { get; set; }
        }
    }
}

