﻿using RoomsDataMonitor.Common.Json;
using System;
using System.Linq;

namespace RoomsDataMonitor.DataAccess
{
    public class DataAccessProvider
    {
        public IDataAccess DataAccess { get; }

        public DataAccessProvider(IDataAccess dataAccess)
        {
            DataAccess = dataAccess;
        }
    }
}
