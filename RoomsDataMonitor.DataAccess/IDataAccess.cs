﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomsDataMonitor.DataAccess
{
    public interface IDataAccess
    {
        IQueryable<T> GetQueryable<T>();
        void Insert<T>(T value);
    }
}
