﻿using MongoDB.Driver;
using RoomsDataMonitor.DataAccess;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RoomsDataMonitor.DataAccess.MongoDB
{
    public class DataAccessMongoDB : IDataAccess
    {
        private BlockingCollection<dynamic> insertBuffer = new BlockingCollection<dynamic>();
        private IMongoDatabase db;
        private const string DataBaseName = "RoomsMonitorRawData";
        private const string DataBaseCollentionName = "RoomsMonitorRawDataCollection";
        public DataAccessMongoDB()
        {
            var mongo = new MongoClient();
            db = mongo.GetDatabase(DataBaseName);
            Task.Run(() =>
            {
                foreach (var e in insertBuffer.GetConsumingEnumerable())
                {
                    db.GetType()
                    .GetMethod(nameof(db.GetCollection))
                    .MakeGenericMethod(e.Type)
                    .Invoke(db, new object[] { DataBaseCollentionName, null })
                    .InsertOne(e.Value, options: null, cancellationToken: default(CancellationToken));
                }
            });
        }

        public IQueryable<T> GetQueryable<T>()
        {
            return db.GetCollection<T>(DataBaseCollentionName).AsQueryable();
        }

        public void Insert<T>(T value)
        {
            insertBuffer.Add(new { Type = typeof(T), Value = value });
        }
    }
}
