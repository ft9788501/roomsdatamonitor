﻿using System;
using System.Threading.Tasks;

namespace RoomsDataMonitor.Destinations
{
    public interface IDestination
    {
        void Send(string msg);
    }
}
