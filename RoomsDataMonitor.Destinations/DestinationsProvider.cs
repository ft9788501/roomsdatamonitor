﻿using System;
using System.Linq;

namespace RoomsDataMonitor.Destinations
{
    public class DestinationsProvider
    {
        public IDestination Destination { get; }

        public DestinationsProvider(IDestination destination)
        {
            Destination = destination;
        }
    }
}
