﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoomsDataMonitor.Common.Json
{
    public class MixpanelRawData
    {
        public bool force_refresh { get; set; }
        public MixpanelRawDataEvent[] event_list { get; set; }
        public string message { get; set; }
        public string oldest_event_timestamp { get; set; }
    }
    public class MixpanelRawDataEvent
    {
        [BsonId]
        public ObjectId Id { get; set; }
        [JsonProperty(PropertyName = "$ts")]
        public long ts { get; set; }
        public string @event { get; set; }
        public MixpanelRawDataEventProperties properties { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
    public class MixpanelRawDataEventProperties
    {
        public string endPoint { get; set; }
        public string companyID { get; set; }
        public string monitorNumber { get; set; }
        public string manufacturer { get; set; }
        public string phoneBeta { get; set; }
        public string upcomingListHeight { get; set; }
        [JsonProperty(PropertyName = "$screen_width")]
        public int screen_width { get; set; }
        [JsonProperty(PropertyName = "$ios_device_model")]
        public string ios_device_model { get; set; }
        [JsonProperty(PropertyName = "$created")]
        public DateTime created { get; set; }
        [JsonProperty(PropertyName = "$answerCalls")]
        public string answerCalls { get; set; }
        public string id { get; set; }
        public string tier { get; set; }
        public string callSwitchPermission { get; set; }
        [JsonProperty(PropertyName = "$name")]
        public string _name { get; set; }
        public string iOSUser { get; set; }
        public string appVersion { get; set; }
        public string name { get; set; }
        [JsonProperty(PropertyName = "$app_release")]
        public string app_release { get; set; }
        [JsonProperty(PropertyName = "$ios_version")]
        public string ios_version { get; set; }
        [JsonProperty(PropertyName = "$os")]
        public string os { get; set; }
        public string rcExtensionId { get; set; }
        public string mp_country_code { get; set; }
        public string company { get; set; }
        public string Freyja { get; set; }
        [JsonProperty(PropertyName = "$ios_app_version")]
        public string ios_app_version { get; set; }
        [JsonProperty(PropertyName = "$os_version")]
        public string os_version { get; set; }
        [JsonProperty(PropertyName = "$wifi")]
        public bool wifi { get; set; }
        [JsonProperty(PropertyName = "$email")]
        public string email { get; set; }
        public int time { get; set; }
        public string companyCategory { get; set; }
        public string distinct_id { get; set; }
        public string accountType { get; set; }
        public string AndroidUser { get; set; }
        [JsonProperty(PropertyName = "$screen_height")]
        public int screen_height { get; set; }
        [JsonProperty(PropertyName = "$ios_app_release")]
        public string ios_app_release { get; set; }
        public string userLanguage { get; set; }
        [JsonProperty(PropertyName = "$app_version_string")]
        public string app_version_string { get; set; }
        public string partnerBrand { get; set; }
        public string telephonyToggle { get; set; }
        [JsonProperty(PropertyName = "$model")]
        public string model { get; set; }
        public string rcAccountId { get; set; }
        public string mp_lib { get; set; }
        public string directorySize { get; set; }
        [JsonProperty(PropertyName = "$last_name")]
        public string last_name { get; set; }
        public string installRCMobileApp { get; set; }
        public string installRCMeetingsApp { get; set; }
        public string videoService { get; set; }
        [JsonProperty(PropertyName = "$first_name")]
        public string first_name { get; set; }
        public long mp_processing_time_ms { get; set; }
        public string ringOutPickUpNumber { get; set; }
        [JsonProperty(PropertyName = "$insert_id")]
        public string insert_id { get; set; }
        public string duration { get; set; }
        public string tapButton { get; set; }
        public string buttonOrder { get; set; }
        [JsonProperty(PropertyName = "$city")]
        public string city { get; set; }
        [JsonProperty(PropertyName = "$region")]
        public string region { get; set; }
        public string type { get; set; }
        [JsonProperty(PropertyName = "$browser")]
        public string browser { get; set; }
        [JsonProperty(PropertyName = "$browser_version")]
        public int browser_version { get; set; }
        [JsonProperty(PropertyName = "$current_url")]
        public string current_url { get; set; }
        [JsonProperty(PropertyName = "$device_id")]
        public string device_id { get; set; }
        [JsonProperty(PropertyName = "$initial_referrer")]
        public string initial_referrer { get; set; }
        [JsonProperty(PropertyName = "$initial_referring_domain")]
        public string initial_referring_domain { get; set; }
        [JsonProperty(PropertyName = "$lib_version")]
        public string lib_version { get; set; }
        [JsonProperty(PropertyName = "$user_id")]
        public string user_id { get; set; }
        public string account { get; set; }
        [JsonProperty(PropertyName = "$endpointId")]
        public string endpointId { get; set; }
        public string extensionNumber { get; set; }
        public string meetingID { get; set; }
        public string microphone { get; set; }
        public string participantId { get; set; }
        public string source { get; set; }
        public string version { get; set; }
        public bool video { get; set; }
        public string action { get; set; }
        public bool endedWithJoinedAudio { get; set; }
        public bool wasAudioJoined { get; set; }
        public string build { get; set; }
        public bool from_background { get; set; }
        public string referring_application { get; set; }
        public string url { get; set; }
        public string status { get; set; }
        public string audioInput { get; set; }
        public string audioOutput { get; set; }
        public string camera { get; set; }
        public string hostAppVersion { get; set; }
        public string hostNetwork { get; set; }
        public string monitor1display { get; set; }
        public string monitor1resolution { get; set; }
        public string monitor2display { get; set; }
        public string monitor2resolution { get; set; }
        public string actualDuration { get; set; }
        public string unreadFilterToggle { get; set; }
        public string previous_build { get; set; }
        public string previous_version { get; set; }
        public string errorType { get; set; }
    }
}
