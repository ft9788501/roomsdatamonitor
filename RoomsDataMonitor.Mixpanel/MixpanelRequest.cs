﻿using Newtonsoft.Json;
using RoomsDataMonitor.Common.Json;
using RoomsDataMonitor.Mixpanel.Extensions;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace RoomsDataMonitor.Mixpanel
{
    internal static class MixpanelRequest
    {
        public static async Task<MixpanelRawDataEvent[]> GetExportData(DateTime startTime, DateTime endTime)
        {
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(60);
                    var httpResponseMessage = await httpClient.GetAsync(RequestURL.ExportUrl(startTime, endTime));
                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        using (var responsestream = await httpResponseMessage.Content.ReadAsStreamAsync())
                        using (var streamreader = new StreamReader(responsestream))
                        {
                            var result = $"[{string.Join(",", (await streamreader.ReadToEndAsync()).Split(new string[] { "\n" }, StringSplitOptions.None))}]";
                            return JsonConvert.DeserializeObject<MixpanelRawDataEvent[]>(result);
                        }
                    }
                    throw new Exception($"ErrorStatusCode:{httpResponseMessage.StatusCode}");
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error:{ex.Message}");
                Console.WriteLine("Retry in 1 second...");
                await Task.Delay(1000);
                return await GetExportData(startTime, endTime);
            }
        }
        public static async Task<MixpanelRawData> GetLiveData(DateTime startTime)
        {
            return await GetLiveData(startTime.ToUnixLocalTimestamp());
        }
        public static async Task<MixpanelRawData> GetLiveData(long startTime)
        {
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(10);
                    var httpResponseMessage = await httpClient.GetAsync(RequestURL.LiveUrl(startTime));
                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        using (var responsestream = await httpResponseMessage.Content.ReadAsStreamAsync())
                        using (var streamreader = new StreamReader(responsestream))
                        {
                            var result = await streamreader.ReadToEndAsync();
                            return JsonConvert.DeserializeObject<MixpanelRawData>(result);
                        }

                    }
                    throw new Exception($"ErrorStatusCode:{httpResponseMessage.StatusCode}");
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error:{ex.Message}");
                Console.WriteLine("Retry in 1 minute...");
                await Task.Delay(1000 * 60);
                return await GetLiveData(startTime);
            }
        }
    }
}
