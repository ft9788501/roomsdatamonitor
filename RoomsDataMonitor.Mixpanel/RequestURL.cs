﻿using RoomsDataMonitor.Mixpanel.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace RoomsDataMonitor.Mixpanel
{
    internal static class RequestURL
    {
        const string ApiKey = "2147ce57a268900b5820f5f1466cbc00";
        const string ApiSecret = "829dec344dd9a4b7ff5911a78b952b1c";
        const string Token = "89912cb3fed67c2444704ad13bef4ca6";
        public static string LiveUrl(long startTime)
        {
            var parameters = new NameValueCollection();
            parameters.AddIfNotIsNullOrWhiteSpace("project_id", "1369629");
            parameters.AddIfNotIsNullOrWhiteSpace("start_time", startTime.ToString());
            parameters.AddIfNotIsNullOrWhiteSpace("client_version", "3");
            //parameters.AddIfNotIsNullOrWhiteSpace("distinct_id", "1369629");
            //parameters.AddIfNotIsNullOrWhiteSpace("user_id", "1805533");

            var endpoint = new Endpoint().Create(ApiKey, ApiSecret)
                                         .ForMethod(MethodEnum.Live)
                                         .WithParameters(parameters)
                                         .Build();
            return endpoint;
        }

        public static string ExportUrl(DateTime from, DateTime to, ICollection<string> events = null, string where = "", string bucket = "")
        {
            var parameters = new NameValueCollection();
            parameters.AddIfNotIsNullOrWhiteSpace("from_date", from.ToString("yyyy-MM-dd"));
            parameters.AddIfNotIsNullOrWhiteSpace("to_date", to.ToString("yyyy-MM-dd"));
            parameters.AddIfNotIsNullOrWhiteSpace("where", where);

            var endpoint = new Endpoint().Create(ApiKey, ApiSecret)
                                         .ForMethod(MethodEnum.Export)
                                         .WithParameters(parameters)
                                         .Build();
            return endpoint;
        }

        public static string EngageUrl(string where = "", string sessionId = "", int page = 0)
        {
            var parameters = new NameValueCollection();
            parameters.AddIfNotIsNullOrWhiteSpace("where", where);
            parameters.AddIfNotIsNullOrWhiteSpace("session_id", sessionId);
            parameters.AddIfNotIsNullOrWhiteSpace("page", page.ToString());

            var endpoint = new Endpoint().Create(ApiKey, ApiSecret)
                                         .ForMethod(MethodEnum.Engage)
                                         .WithParameters(parameters)
                                         .Build();
            return endpoint;
        }

        public static string SegmentationUrl(string eventName, DateTime from, DateTime to)
        {
            var parameters = new NameValueCollection();
            parameters.AddIfNotIsNullOrWhiteSpace("event", eventName);
            parameters.AddIfNotIsNullOrWhiteSpace("from_date", from.ToString("yyyy-MM-dd"));
            parameters.AddIfNotIsNullOrWhiteSpace("to_date", to.ToString("yyyy-MM-dd"));

            var endpoint = new Endpoint().Create(ApiKey, ApiSecret)
                                         .ForMethod(MethodEnum.Segmentation)
                                         .WithParameters(parameters)
                                         .Build();
            return endpoint;
        }
    }
}
