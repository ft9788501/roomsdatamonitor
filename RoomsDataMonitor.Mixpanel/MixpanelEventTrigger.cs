﻿using Newtonsoft.Json;
using RoomsDataMonitor.Common.Json;
using RoomsDataMonitor.Mixpanel.Extensions;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace RoomsDataMonitor.Mixpanel
{
    public class MixpanelEventTrigger
    {
        public event EventHandler<MixpanelRawDataEvent> ReceiveMixpanelRawDataEvent;
        protected virtual void OnReceiveMixpanelRawDataEvent(MixpanelRawDataEvent mixpanelRawDataEvent)
        {
            ReceiveMixpanelRawDataEvent?.Invoke(this, mixpanelRawDataEvent);
        }

        private BlockingCollection<MixpanelRawDataEvent> mixpanelRawDataEvent = new BlockingCollection<MixpanelRawDataEvent>();
        private long lastTime = DateTime.Today.ToUnixLocalTimestamp();
        public MixpanelEventTrigger()
        {
        }

        public void StartGrab(int interval = 100)
        {
            Task.Run(async () =>
            {
                var data = await MixpanelRequest.GetLiveData(lastTime);
                while (true)
                {
                    if (data.event_list.Length > 0)
                    {
                        lastTime = data.event_list[data.event_list.Length - 1].ts;
                        foreach (var e in data.event_list)
                        {
                            mixpanelRawDataEvent.Add(e);
                        }
                    }
                    await Task.Delay(interval);
                    data = await MixpanelRequest.GetLiveData(lastTime);
                }
            });
            Task.Run(() =>
            {
                foreach (var e in mixpanelRawDataEvent.GetConsumingEnumerable())
                {
                    try
                    {
                        OnReceiveMixpanelRawDataEvent(e);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine($"Event Deserialize Error:{ex.Message}");
                    }
                }
            });
        }
    }
}
