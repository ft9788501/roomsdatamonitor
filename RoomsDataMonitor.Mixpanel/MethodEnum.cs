﻿using System.ComponentModel;

namespace RoomsDataMonitor.Mixpanel
{
    internal enum MethodEnum
    {
        [Description("engage")]
        Engage,

        [Description("export")]
        Export,

        [Description("segmentation")]
        Segmentation,

        [Description("live")]
        Live
    }
}
