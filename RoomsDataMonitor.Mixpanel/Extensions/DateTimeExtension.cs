﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomsDataMonitor.Mixpanel.Extensions
{
    internal static class DateTimeExtension
    {
        public static double ToUnixTimestamp(this DateTime dateTime)
        {
            var unixEpoch = new DateTime(1970, 1, 1);

            if (dateTime < unixEpoch) return 0;

            return Math.Floor(dateTime.Subtract(unixEpoch).TotalSeconds);
        }
        public static long ToUnixLocalTimestamp(this DateTime dateTime)
        {
            var unixEpoch = new DateTime(1970, 1, 1);

            if (dateTime < unixEpoch) return 0;

            return (long)(dateTime - unixEpoch.ToLocalTime()).TotalMilliseconds;
        }
        public static DateTime ToDateTime(this long unixLocalTimestamp)
        {
            return new DateTime(1970, 1, 1, 8, 0, 0).AddMilliseconds(unixLocalTimestamp);
        }
    }
}
