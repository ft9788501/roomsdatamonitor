﻿using RoomsDataMonitor.Common.Json;
using RoomsDataMonitor.DataAccess;
using RoomsDataMonitor.DataAccess.MongoDB;
using RoomsDataMonitor.Destinations;
using RoomsDataMonitor.Destinations.Glip;
using RoomsDataMonitor.Mixpanel;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace RoomsDataMonitor.Core
{
    class Program
    {
        static DataAccessProvider dataBaseProvider = new DataAccessProvider(new DataAccessMongoDB());
        static DestinationsProvider destinationsProvider = new DestinationsProvider(new DestinationGlip());
        static MixpanelEventTrigger mixpanelEventTrigger = new MixpanelEventTrigger();
        static void Main(string[] args)
        {
            mixpanelEventTrigger.ReceiveMixpanelRawDataEvent += (s, e) =>
            {
                dataBaseProvider.DataAccess.Insert(e);
                destinationsProvider.Destination.Send(e.@event);
                Console.WriteLine($"Grab event:{e.@event}");
            };
            Console.WriteLine("Grab started");
            mixpanelEventTrigger.StartGrab();
            Console.ReadKey();
        }
    }
}
